//Import some code we need
var React = require('react-native');
var moment = require('moment');

var AppRegistry = React.AppRegistry;
var Text = React.Text;
var View = React.View;
var StyleSheet = React.StyleSheet;
var DayItem = require('./src/day-item');

var DAYS = ['Sunday','Monday','Tuesday','Wendesday','Thursday','Friday','Saturday'];

// Create a react component
var WeekDays = React.createClass({
  render: function() {
    return <View style={styles.container}>
      {this.days()}
    </View>
  },
  days: function() {
    
    var daysItems = [];

    for (var i = 0;i < 7 ; i++ ) {
      var day = moment().add(i,'days').format('dddd');
      daysItems.push(
        <DayItem day={day} daysUntil={i}/>
      )
    }
    return daysItems
  }
});

//Style react comopnent
var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center', //move stuff height wise
    alignItems: 'center' //moves stuff width wise
  }
});


// Show the react component on the screen
AppRegistry.registerComponent('weekdays', function() {
  return WeekDays;
});
